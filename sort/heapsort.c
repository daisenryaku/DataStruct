/*
 ============================================================================
 Name        : heapsort.c
 Author      : daisenryaku
 Time     :2016.5.1
 Copyright   : copyright (c) all rights reversed
 Description : 对整型数组使用堆排序
 ============================================================================
 */

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void Swap(int* a,int* b){
	int temp;
	temp = *a;
	*a = *b;
	*b = temp;
	return;
}

void Adjust(int* L,int i,int n){
	int j;
	int temp = L[i];
	for(j=2*i;j<=n;j*=2){
		if(j<n && L[j]<L[j+1]) j++;
		if(temp>L[j]) break;
		L[i] = L[j];
		i=j;
	}
	L[i] = temp;
	return;
}

void HeapSort(int* L,int n){
	int i;
	for(i=n/2;i>0;i--){
		Adjust(L,i,n);
	}

	for(i=n;i>0;i--){
		Swap(&L[1],&L[i]);
		Adjust(L,1,i-1);
	}
	return;
}

int main(){
	int i,n;
	srand((unsigned)time(NULL));
	printf("请输入要排序的数字个数:");
	scanf("%d",&n);
	int L[n+1];

	for(i=1;i<=n;i++){
		L[i] = rand() % 100;
	}

	HeapSort(L,n);

	printf("after sorted:");
	for(i=1;i<=n;i++){
		printf("%4d",L[i]);
	}
}
