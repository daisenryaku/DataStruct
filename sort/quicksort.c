/*
 * quicksort.c
 *对整型数组使用快速排序
 *  Created on: 2016年4月29日
 *      Author: z
 */
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
/*MAX为数组长度*/
#define MAX 10

void 	Quicksort(int* L,int low,int high);
void Swap(int* L,int low,int high);
int Partition(int* L,int low,int high);

void 	Quicksort(int* L,int low,int high){
	int k;
	while(L && low < high){
		k = Partition(L,low,high);
		Quicksort(L,low,k-1);
		low = k+1;
	}

}

void Swap(int* L,int low,int high){
	int temp;
	temp = L[low];
	L[low] = L[high];
	L[high] = temp;
	return;
}

int Partition(int* L,int low,int high){
	int m = low + (high - low) / 2;
	/*三者取中法*/
	if(m < high){Swap(L,m,high);}
	if(low < high){Swap(L,low,high);}
	if(m < low){Swap(L,m,low);}

	int k=L[low],temp=L[low];

	while(low < high ){
		while(low < high && L[high] >= k ) high--;
		L[low] = L[high];
		while(low < high && L[low] <= k ) low++;
		L[high] = L[low];
		}
	L[low] = temp;
	return low;
}

int main(){
	int a[MAX],i;
	/*随机生成数组并打印*/
	srand((unsigned)time(NULL));
	for(i=0;i<MAX;i++){
		a[i]=rand() % 100 ;
	}
	for(i=0;i<MAX;i++){
		printf("%d\t",a[i]);
	}
	printf("\n");

	/*快速排序并打印*/
	Quicksort(a,0,MAX-1);
	for(i=0;i<MAX;i++){
		printf("%d\t",a[i]);
	}
	return 0;
}


